/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.krorawitt.midterm;

import javax.swing.DefaultListModel;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author WINDOWS 10
 */
public class PointofSell extends javax.swing.JPanel {

    private DefaultListModel model;
    private int index = -1;

    /**
     * Creates new form PointofSell
     */
    public PointofSell() {
        Productservice.load();
        initComponents();
        model = new DefaultListModel();
        lstProduct.setModel(model);
        refresh();
        disbleEditUI();
        
    }

    public void disbleEditUI() {
        edtId.setEnabled(false);
        edtName.setEnabled(false);
        edtBrand.setEnabled(false);
        edtPrice.setEnabled(false);
        edtAmount.setEnabled(false);
        bntSave1.setEnabled(false);
        
    }

    public void enableEditUI() {
        edtId.setEnabled(true);
        edtName.setEnabled(true);
        edtBrand.setEnabled(true);
        edtPrice.setEnabled(true);
        edtAmount.setEnabled(true);
        bntSave1.setEnabled(true);
        
    }

    public void refresh() {
        model.clear();
        model.addAll(Productservice.getProducts());
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel2 = new javax.swing.JLabel();
        jPanel1 = new javax.swing.JPanel();
        edtId1 = new javax.swing.JLabel();
        edtId = new javax.swing.JTextField();
        edtName1 = new javax.swing.JLabel();
        edtName = new javax.swing.JTextField();
        edtBrand1 = new javax.swing.JLabel();
        edtBrand = new javax.swing.JTextField();
        edtPrice1 = new javax.swing.JLabel();
        edtPrice = new javax.swing.JTextField();
        edtAmount1 = new javax.swing.JLabel();
        edtAmount = new javax.swing.JTextField();
        bntSave1 = new javax.swing.JButton();
        bntInfo1 = new javax.swing.JButton();
        bntDelete1 = new javax.swing.JButton();
        bntClear1 = new javax.swing.JButton();
        bntNew1 = new javax.swing.JButton();
        bntEdit1 = new javax.swing.JButton();
        jScrollPane1 = new javax.swing.JScrollPane();
        lstProduct = new javax.swing.JList<>();
        edtTotalPrice1 = new javax.swing.JLabel();
        edtTotalPrice = new javax.swing.JTextField();
        edtTotalAmount1 = new javax.swing.JLabel();
        edtTotalAmount = new javax.swing.JTextField();

        setBackground(new java.awt.Color(153, 255, 255));

        jLabel2.setFont(new java.awt.Font("Angsana New", 1, 24)); // NOI18N
        jLabel2.setText("ร้านของภูผาเองค้าบ");

        jPanel1.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        edtId1.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        edtId1.setText("ID:");

        edtId.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                edtIdActionPerformed(evt);
            }
        });

        edtName1.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        edtName1.setText("Name:");

        edtName.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                edtNameActionPerformed(evt);
            }
        });

        edtBrand1.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        edtBrand1.setText("Brand:");

        edtBrand.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                edtBrandActionPerformed(evt);
            }
        });

        edtPrice1.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        edtPrice1.setText("Price:");

        edtPrice.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                edtPriceActionPerformed(evt);
            }
        });

        edtAmount1.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        edtAmount1.setText("Amount:");

        edtAmount.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                edtAmountActionPerformed(evt);
            }
        });

        bntSave1.setText("Save");
        bntSave1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bntSave1ActionPerformed(evt);
            }
        });

        bntInfo1.setText("Info");
        bntInfo1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bntInfo1ActionPerformed(evt);
            }
        });

        bntDelete1.setText("Delete");
        bntDelete1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bntDelete1ActionPerformed(evt);
            }
        });

        bntClear1.setText("Clear");
        bntClear1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bntClear1ActionPerformed(evt);
            }
        });

        bntNew1.setText("New");
        bntNew1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bntNew1ActionPerformed(evt);
            }
        });

        bntEdit1.setText("Edit");
        bntEdit1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bntEdit1ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(25, 25, 25)
                        .addComponent(edtId1)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(edtId, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(23, 23, 23)
                        .addComponent(edtName1)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(edtName, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(edtBrand1)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(edtBrand, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 22, Short.MAX_VALUE))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(edtPrice1)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(edtPrice, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(10, 10, 10)
                        .addComponent(edtAmount1)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(edtAmount, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(bntNew1, javax.swing.GroupLayout.PREFERRED_SIZE, 70, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)))
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(bntEdit1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addGap(9, 9, 9)
                        .addComponent(bntDelete1, javax.swing.GroupLayout.PREFERRED_SIZE, 70, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(80, 80, 80)
                        .addComponent(bntSave1, javax.swing.GroupLayout.PREFERRED_SIZE, 70, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGap(10, 10, 10)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(bntClear1, javax.swing.GroupLayout.PREFERRED_SIZE, 70, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(bntInfo1, javax.swing.GroupLayout.PREFERRED_SIZE, 70, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap())
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(edtId1)
                    .addComponent(edtId, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(edtName1)
                    .addComponent(edtName, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(edtBrand1)
                    .addComponent(edtBrand, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(bntSave1)
                    .addComponent(bntInfo1))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 29, Short.MAX_VALUE)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(bntDelete1)
                    .addComponent(edtPrice1)
                    .addComponent(edtPrice, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(edtAmount1)
                    .addComponent(edtAmount, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(bntClear1)
                    .addComponent(bntNew1)
                    .addComponent(bntEdit1))
                .addGap(27, 27, 27))
        );

        jScrollPane1.setViewportView(lstProduct);

        edtTotalPrice1.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        edtTotalPrice1.setText("Total Price :");

        edtTotalPrice.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                edtTotalPriceActionPerformed(evt);
            }
        });

        edtTotalAmount1.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        edtTotalAmount1.setText("Total Amount :");

        edtTotalAmount.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                edtTotalAmountActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jLabel2)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(edtTotalPrice1)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(edtTotalPrice, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(edtTotalAmount1)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(edtTotalAmount, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(jScrollPane1))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(edtTotalAmount1)
                        .addComponent(edtTotalAmount, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(edtTotalPrice1)
                        .addComponent(edtTotalPrice, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(jLabel2))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 260, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(6, 6, 6))
        );
    }// </editor-fold>//GEN-END:initComponents

    private void edtIdActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_edtIdActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_edtIdActionPerformed

    private void edtNameActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_edtNameActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_edtNameActionPerformed

    private void edtBrandActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_edtBrandActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_edtBrandActionPerformed

    private void edtPriceActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_edtPriceActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_edtPriceActionPerformed

    private void edtAmountActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_edtAmountActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_edtAmountActionPerformed

    private void bntSave1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bntSave1ActionPerformed
        if (index < 0) {
            String ID = edtId.getText();
            String Name = edtName.getText();
            String Brand = edtBrand.getText();
            double Price = Double.parseDouble(edtPrice.getText());
            int Amount = Integer.parseInt(edtAmount.getText());
            Product product = new Product(ID, Name, Brand, Price, Amount);
            Productservice.addProduct(product);
            refresh();
            disbleEditUI();
            
        } else {
            String ID = edtId.getText();
            String Name = edtName.getText();
            String Brand = edtBrand.getText();
            double Price = Double.parseDouble(edtPrice.getText());
            int Amount = Integer.parseInt(edtAmount.getText());
            Product product = new Product(ID, Name, Brand, Price, Amount);
            Productservice.updateProduct(index, product);
            
        }
        refresh();
        disbleEditUI();
    }//GEN-LAST:event_bntSave1ActionPerformed
    private void btnClearActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnClearActionPerformed
        edtId.setText("");
        edtName.setText("");
        edtBrand.setText("");
        edtPrice.setText("");
        edtAmount.setText("");
        edtTotalPrice.setText("");
        edtAmount.setText("");
        edtId.requestFocus();
        
    }//GEN-LAST:event_btnClearActionPerformed
    private void bntInfo1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bntInfo1ActionPerformed
        double totalPrice = 0;
        int totalAmount = 0;

        for (Product x : Productservice.getProducts()) {
            totalPrice += (x.getOrderPrice()* x.getOrderAmount());
            totalAmount += x.getOrderAmount();
        }

        edtTotalPrice.setText("" + totalPrice);
        edtTotalAmount.setText("" + totalAmount);

    }//GEN-LAST:event_bntInfo1ActionPerformed

    private void bntNew1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bntNew1ActionPerformed
        enableEditUI();
        edtId.requestFocus();
        index = -1;
    }//GEN-LAST:event_bntNew1ActionPerformed

    private void bntDelete1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bntDelete1ActionPerformed
        index = lstProduct.getSelectedIndex();
        if (index < 0) {
            return;
        }
        Productservice.removeProduct(index);
        refresh();
        
    }//GEN-LAST:event_bntDelete1ActionPerformed

    private void edtTotalPriceActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_edtTotalPriceActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_edtTotalPriceActionPerformed

    private void edtTotalAmountActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_edtTotalAmountActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_edtTotalAmountActionPerformed

    private void bntEdit1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bntEdit1ActionPerformed
        index = lstProduct.getSelectedIndex();
        if (lstProduct.getSelectedIndex() < 0) {
            return;
        }
        Product product = Productservice.getProduct(index);
        edtId.setText(product.getOrderId());
        edtName.setText(product.getOrderName());
        edtBrand.setText(product.getBrandName());
        edtPrice.setText("" + product.getOrderPrice());
        edtAmount.setText("" + product.getOrderAmount());
        enableEditUI();
        edtId.requestFocus();
        
    }//GEN-LAST:event_bntEdit1ActionPerformed

    private void bntClear1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bntClear1ActionPerformed
        Productservice.removeAllProduct();
        refresh();
        edtId.setText("");
        edtName.setText("");
        edtBrand.setText("");
        edtPrice.setText("");
        edtAmount.setText("");
        edtTotalPrice.setText("");
        edtTotalAmount.setText("");
        edtId.requestFocus();   
      // TODO add your handling code here:
    }//GEN-LAST:event_bntClear1ActionPerformed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton bntClear1;
    private javax.swing.JButton bntDelete1;
    private javax.swing.JButton bntEdit1;
    private javax.swing.JButton bntInfo1;
    private javax.swing.JButton bntNew1;
    private javax.swing.JButton bntSave1;
    private javax.swing.JTextField edtAmount;
    private javax.swing.JLabel edtAmount1;
    private javax.swing.JTextField edtBrand;
    private javax.swing.JLabel edtBrand1;
    private javax.swing.JTextField edtId;
    private javax.swing.JLabel edtId1;
    private javax.swing.JTextField edtName;
    private javax.swing.JLabel edtName1;
    private javax.swing.JTextField edtPrice;
    private javax.swing.JLabel edtPrice1;
    private javax.swing.JTextField edtTotalAmount;
    private javax.swing.JLabel edtTotalAmount1;
    private javax.swing.JTextField edtTotalPrice;
    private javax.swing.JLabel edtTotalPrice1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JList<String> lstProduct;
    // End of variables declaration//GEN-END:variables
}
