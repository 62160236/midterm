/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.krorawitt.midterm;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author WINDOWS 10
 */
public class Product implements Serializable {
    private String orderId;
    private String orderName;
    private String brandName;
    private Double orderPrice;
    private int orderAmount;

    public Product(String orderId, String orderName, String brandName, Double orderPrice, int orderAmount) {
        this.orderId = orderId;
        this.orderName = orderName;
        this.brandName = brandName;
        this.orderPrice = orderPrice;
        this.orderAmount = orderAmount;
      
    }
    Product(String Id, String Name, String Brand, String Price, String Amount) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public String getOrderId() {

        return orderId;
    }

    public void setOrderId(String orderId) {
     
        this.orderId = orderId;
    }

    public String getOrderName() {
       
        return orderName;
    }

    public void setOrderName(String orderName) {
       
        this.orderName = orderName;
    }

    public String getBrandName() {
    
        return brandName;
    }

    public void setBrandName(String brandName) {

        this.brandName = brandName;
    }

    public Double getOrderPrice() {

        return orderPrice;
    }

    public void setOrderPrice(Double orderPrice) {

        this.orderPrice = orderPrice;
    }

    public int getOrderAmount() {

        return orderAmount;
    }
     public void setOrderAmount(int orderAmount) {

        this.orderAmount = orderAmount;
    }

    @Override
    public String toString() {
        return "orderId=" + orderId + ", orderName=" + orderName + ", brandName=" + brandName + ", orderPrice=" + orderPrice + ", orderAmount=" + orderAmount;
    }
}
